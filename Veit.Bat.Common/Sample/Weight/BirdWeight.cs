﻿using System;
using System.Runtime.Serialization;
using Veit.Bat.Common.Units;

namespace Veit.Bat.Common.Sample.Weight
{
    [DataContract]
    public class BirdWeight : Weight
    {
        public BirdWeight()
           : this(0)
        {
        }

        public BirdWeight(double grams = 0)
           : this(grams, WeightUnits.G)
        {
        }

        public BirdWeight(double weight, DateTimeOffset timeStamp)
           : base(weight, timeStamp)
        {
        }

        public BirdWeight(Weight weight)
           : this(weight.AsG)
        {
        }
        public BirdWeight(DifferenceWeight weight) :
           base(weight.Difference == DifferenceMode.Decreased ? weight * -1 : weight)
        {
        }

        public BirdWeight(double? grams)
           : this(grams ?? 0)
        {
        }

        public BirdWeight(double weight, WeightUnits units) :
           base(weight, units)
        {
        }

        [DataMember]
        public Sex Sex { get; set; }

        #region Overrides of Weight

        /// <summary>
        /// Returns a string that represents the current object.
        /// </summary>
        /// <returns>
        /// A string that represents the current object.
        /// </returns>
        public override string ToString()
        {
            return $"{base.ToString()} {Sex}";
        }

        #endregion
    }
}
