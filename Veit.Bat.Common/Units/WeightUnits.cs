﻿namespace Veit.Bat.Common.Units
{
   public enum WeightUnits
   {
      KG,
      G,
      LB,
   };
}
