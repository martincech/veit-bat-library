﻿using System;
using Xunit;

namespace Veit.Bat.Common.Tests.UnitTests
{
    public class PercentTests
    {
        private double number;
        private Percent uut;
        private readonly Percent greaterUut;
        private readonly Percent sameUut;
        private const double DEFAULT_NUMBER = 10;

        public PercentTests()
        {
            uut = new Percent(DEFAULT_NUMBER);
            greaterUut = new Percent(DEFAULT_NUMBER * 2);
            sameUut = new Percent(DEFAULT_NUMBER);
            number = 100.0;
        }

        [Fact]
        public void OutOfRange_ForNegativePercent()
        {
            var percentNumber = new Percent();
#pragma warning disable IDE0039 // Use local function
            Action act = () => { percentNumber = new Percent(-5); };
#pragma warning restore IDE0039 // Use local function
            Assert.Throws<ArgumentOutOfRangeException>(act);
        }

        [Fact]
        public void OutOfRange_ForMoreThan100()
        {
            var percentNumber = new Percent();
#pragma warning disable IDE0039 // Use local function
            Action act = () => { percentNumber = new Percent(101); };
#pragma warning restore IDE0039 // Use local function
            Assert.Throws<ArgumentOutOfRangeException>(act);
        }

        [Fact]
        public void Valid_For_0_And_100()
        {
            uut = new Percent(0);
            Assert.Equal(0, (double)uut);
            uut = new Percent(100);
            Assert.Equal(100, (double)uut);

        }
        [Fact]
        public void Double_Plus_Percent()
        {
            Assert.Equal(110, number + uut);
        }

        [Fact]
        public void Percent_Plus_Double()
        {
            Assert.Equal(110, uut + number);
        }

        [Fact]
        public void Double_Minus_Percent()
        {
            Assert.Equal(90, number - uut);
        }

        [Fact]
        public void Percent_Minus_Double()
        {
            Assert.Equal(-90, uut - number);
        }

        [Fact]
        public void Double_Times_Percent()
        {
            Assert.Equal(1000, number * uut);
        }

        [Fact]
        public void Percent_Times_Double()
        {
            Assert.Equal(1000, uut * number);
        }

        [Fact]
        public void Double_Div_Percent()
        {
            Assert.Equal(10, number / uut);
        }

        [Fact]
        public void Percent_Div_Double()
        {
            Assert.Equal(0.1, uut / number);
        }


        //TODO testy na porovnání

        [Fact]
        public void Percent_Equals_Percent()
        {
            var percent = uut;
            Assert.True(uut == sameUut);
            Assert.True(uut == percent);
        }

        [Fact]
        public void Percent_Not_Equals_Percent()
        {
            Assert.True(uut != greaterUut);
        }

        [Fact]
        public void Percent_Equals_Double()
        {
            number = DEFAULT_NUMBER;
            Assert.True(uut == number);
            Assert.True(number == uut);
        }

        [Fact]
        public void Percent_Not_Equals_Double()
        {
            Assert.True(uut != number);
            Assert.True(number != uut);
        }

        [Fact]
        public void Percent_CompareTo_Percent()
        {
            Assert.Equal(1, uut.CompareTo(new Percent(DEFAULT_NUMBER - 1)));
            Assert.Equal(0, uut.CompareTo(new Percent(DEFAULT_NUMBER)));
            Assert.Equal(-1, uut.CompareTo(new Percent(DEFAULT_NUMBER + 1)));
        }

        [Fact]
        public void Percent_CompareTo_Double()
        {
            Assert.Equal(1, uut.CompareTo(DEFAULT_NUMBER - 1));
            Assert.Equal(0, uut.CompareTo(DEFAULT_NUMBER));
            Assert.Equal(-1, uut.CompareTo(DEFAULT_NUMBER + 1));
        }
    }
}
