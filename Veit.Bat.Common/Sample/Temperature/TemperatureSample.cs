﻿using System;
using System.Runtime.Serialization;
using Veit.Bat.Common.Sample.Base;
using Veit.Bat.Common.Units;
using Veit.Bat.Common.Units.Conversion;

namespace Veit.Bat.Common.Sample.Temperature
{
   [DataContract]
   public class TemperatureSample : TimeStampedSample
   {
      private const int DOUBLE_PRECISION = 5;

      private static double RoundToPrecision(double d)
      {
         return Math.Round(d, DOUBLE_PRECISION);
      }
      public TemperatureSample(double celsius, DateTimeOffset timeStamp)
         : base(RoundToPrecision(celsius), timeStamp)
      {
      }

      public TemperatureSample(double celsius)
         : this(celsius, DateTimeOffset.Now)
      {
      }

      public TemperatureSample(DateTimeOffset timeStamp)
         : this(0, timeStamp)
      {
      }

      public TemperatureSample()
      {
      }

      public double ToUnit(TemperatureUnits toUnit)
      {
         return RoundToPrecision(ConvertTemperature.Convert(Value, TemperatureUnits.Celsius, toUnit));
      }

      public void FromUnit(double value, TemperatureUnits unit)
      {
         Value = RoundToPrecision(ConvertTemperature.Convert(value, unit, TemperatureUnits.Celsius));
      }

   }
}
