﻿using System.Runtime.Serialization;

namespace Veit.Bat.Common.DeviceStatusInfo
{
   [DataContract]
   public class SourceConnectivityInfo
   {
      [DataMember]
      public string Source { get; set; }
      [DataMember]
      public DeviceConnectivityInfo Device { get; set; }
   }
}
