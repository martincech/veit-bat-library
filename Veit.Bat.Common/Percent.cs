﻿using System;
using System.Runtime.Serialization;

namespace Veit.Bat.Common
{
   [DataContract]
   public class Percent : IComparable
   {
      [DataMember]
      private readonly double percent;

      private const double TOLERANCE = 0.0001;

      public Percent(double percent)
      {
         if (percent < 0 || percent > 100)
         {
            throw new ArgumentOutOfRangeException("Invalid percent number");
         }
         this.percent = percent;
      }

      public Percent()
         : this(0)
      {
      }

      private static double PercentOf(Percent perc, double d)
      {
         return (double)perc * d / 100;
      }

      public static double operator +(Percent perc, double d)
      {
         return d + PercentOf(perc, d);
      }

      public static double operator +(double c1, Percent c2)
      {
         return c2 + c1;
      }

      public static double operator -(Percent perc, double d)
      {
         return PercentOf(perc, d) - d;
      }
      public static double operator -(double d, Percent perc)
      {
         return d - PercentOf(perc, d);
      }

      public static double operator *(Percent perc, double d)
      {
         return d * PercentOf(perc, d);
      }
      public static double operator *(double c1, Percent c2)
      {
         return c2 * c1;
      }

      public static double operator /(Percent perc, double d)
      {
         return PercentOf(perc, d) / d;
      }
      public static double operator /(double d, Percent perc)
      {
         return d / PercentOf(perc, d);
      }

      public static bool operator ==(Percent p1, Percent p2)
      {
         if (ReferenceEquals(p1, p2)) return true;
         if (p1 is null) return false;
         if (p2 is null) return false;

         return Math.Abs(p1.percent - p2.percent) < TOLERANCE;
      }

      public static bool operator ==(Percent p, double d)
      {
         if (p is null) return false;
         return Math.Abs(p.percent - d) < TOLERANCE;
      }

      public static bool operator ==(double d, Percent p)
      {
         return p == d;
      }

      public static bool operator !=(Percent p1, Percent p2)
      {
         return !(p1 == p2);
      }

      public static bool operator !=(Percent p, double d)
      {
         return !(p == d);
      }

      public static bool operator !=(double d, Percent p)
      {
         return !(p == d);
      }


      public static implicit operator double(Percent d)
      {
         return d.percent;
      }
      public static implicit operator Percent(double d)
      {
         return new Percent(d);
      }

      #region Equality members

      protected bool Equals(Percent other)
      {
         // ReSharper disable once ImpureMethodCallOnReadonlyValueField
         return percent.Equals(other.percent);
      }

      /// <summary>
      /// Determines whether the specified <see cref="T:System.Object"/> is equal to the current <see cref="T:System.Object"/>.
      /// </summary>
      /// <returns>
      /// true if the specified object  is equal to the current object; otherwise, false.
      /// </returns>
      /// <param name="obj">The object to compare with the current object. </param>
      public override bool Equals(object obj)
      {
         if (obj is null) return false;
         if (ReferenceEquals(this, obj)) return true;
         return obj.GetType() == GetType() && Equals((Percent) obj);
      }

      /// <summary>
      /// Serves as a hash function for a particular type.
      /// </summary>
      /// <returns>
      /// A hash code for the current <see cref="T:System.Object"/>.
      /// </returns>
      public override int GetHashCode()
      {
         unchecked
         {
            return percent.GetHashCode();
         }
      }

      public int CompareTo(object obj)
      {
         var objT = obj.GetType();
         var objTIsDouble = objT == typeof(double);
         if (objT != GetType() && !objTIsDouble)
         {
            throw new InvalidOperationException($"Cannot compare percent with type {objT}");
         }
         var cmpValue = objTIsDouble ? obj : ((Percent)obj).percent;
         var percentCmp = ((IComparable)percent).CompareTo(cmpValue);
         return percentCmp;
      }

      #endregion
   }
}
