﻿using System.Collections.Generic;
using System.Text;

namespace Veit.Bat.Common.Sample.Statistics
{
    public class Histogram
    {
        public Histogram()
        {
            Steps = new Dictionary<int, int>();
        }

        /// <summary>
        /// Weight difference between adjacent steps
        /// </summary>
        public float StepsSize { get; set; }

        /// <summary>
        /// Total number of steps in historgram
        /// </summary>
        public int StepsCount { get; set; }

        /// <summary>
        /// Center value of the histogram
        /// </summary>
        public float Center { get; set; }

        /// <summary>
        /// Dictionary of histogram values. (Key = Step, Value = Count)
        /// </summary>
        public Dictionary<int, int> Steps { get; set; }

        /// <inheritdoc />
        public override string ToString()
        {
            var sb = new StringBuilder(0x020);
            sb.Append("Step size ").Append(StepsSize).Append(" ");
            sb.Append("Count ").Append(StepsCount).Append(" ");
            sb.Append("Center ").Append(Center);
            return sb.ToString();
        }
    }
}
