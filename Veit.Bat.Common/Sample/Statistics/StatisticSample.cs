﻿using System.Runtime.Serialization;
using System.Text;
using Veit.Bat.Common.Sample.Base;

namespace Veit.Bat.Common.Sample.Statistics
{
    [DataContract]
    public class StatisticSample : TimeSample
    {
        /// <summary>
        /// Phone number of the scale
        /// </summary>
        [DataMember]
        public string PhoneNumber { get; set; }

        /// <summary>
        /// Id. number of the scale
        /// </summary>
        [DataMember]
        public long ScaleNumber { get; set; }

        /// <summary>
        /// Name of the scale
        /// </summary>
        [DataMember]
        public string ScaleName { get; set; }

        /// <summary>
        /// Day number
        /// </summary>
        [DataMember]
        public int DayNumber { get; set; }

        /// <summary>
        /// Male data
        /// </summary>
        [DataMember]
        public StatData MaleData { get; set; }

        /// <summary>
        /// Female Data
        /// </summary>
        [DataMember]
        public StatData FemaleData { get; set; }

        public override string ToString()
        {
            var sb = new StringBuilder(0x100);
            sb.Append("Scale ").Append(ScaleName).Append(" ");
            sb.Append("id ").Append(ScaleNumber).Append(" ");
            sb.Append("(Phone: ").Append(PhoneNumber).Append(") ");
            sb.Append("day ").Append(DayNumber).Append(" ");
            sb.Append("date ").Append(TimeStamp).Append(" ");
            sb.Append("MALE:").Append(MaleData?.ToString() ?? "NONE").Append(" ");
            sb.Append("FEMALE: ").Append(FemaleData?.ToString() ?? "NONE");
            return sb.ToString();
        }
    }
}
