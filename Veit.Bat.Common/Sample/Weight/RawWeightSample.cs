﻿using System;
using System.Runtime.Serialization;

namespace Veit.Bat.Common.Sample.Weight
{
   [DataContract]
   public class RawWeightSample : WeightSample
   {
      public RawWeightSample(double rawWeight, DateTimeOffset timeStamp)
         : base(rawWeight, timeStamp)
      {
      }

      public RawWeightSample(double rawWeight)
         : base(rawWeight)
      {
      }

      public RawWeightSample(DateTimeOffset timeStamp)
         : base(timeStamp)
      {
      }

      public RawWeightSample()
      {
      }
   }
}
