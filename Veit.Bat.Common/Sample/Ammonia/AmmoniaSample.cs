﻿using System;
using System.Runtime.Serialization;
using Veit.Bat.Common.Sample.Base;

namespace Veit.Bat.Common.Sample.Ammonia
{
   [DataContract]
   public class AmmoniaSample : TimeStampedSample
   {
      public AmmoniaSample(double value, DateTimeOffset timeStamp)
         : base(value, timeStamp)
      {
      }

      public AmmoniaSample(double value)
         : base(value)
      {
      }

      public AmmoniaSample(DateTimeOffset timeStamp)
         : base(timeStamp)
      {
      }

      public AmmoniaSample()
      {
      }
   }
}
