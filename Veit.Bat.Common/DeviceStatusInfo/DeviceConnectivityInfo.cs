﻿using System.Runtime.Serialization;
using Veit.Bat.Common.Sample.Base;

namespace Veit.Bat.Common.DeviceStatusInfo
{
   [DataContract]
   public class DeviceConnectivityInfo : TimeSample
   {
      [DataMember(Order = 3)]
      public string Type { get; set; }

      [DataMember(Order = 4)]
      public bool Connected { get; set; }
   }
}
