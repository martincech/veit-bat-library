﻿using System;
using Veit.Bat.Common.Sample.Weight;
using Xunit;

namespace Veit.Bat.Common.Tests.UnitTests
{
    public class WeightTests
    {
        #region Private fields and Properties

        private const double REFERENCE_G = 125;
        private const double GRAMS_IN_ONE_LIBRA = 453.59237;
        private static readonly int _originGDecimalPrecision;

        private const double OPERAND1 = 211.2;
        private const double OPERAND2 = 128.5;
        private const double NEGATIVE_OPERAND1 = OPERAND1 * -1;
        private const double NEGATIVE_OPERAND2 = OPERAND2 * -1;

        private double SmallerNumber
        {
            get { return OPERAND1; }
        }

        private double GreaterNumber
        {
            get { return OPERAND1 + 10; }
        }

        #endregion

        #region Initialization

        static WeightTests()
        {
            _originGDecimalPrecision = Weight.GDecimalPrecision;
        }

        public WeightTests()
        {
            //Set origin decimal precision
            Weight.GDecimalPrecision = _originGDecimalPrecision;
        }

        #endregion

        [Fact]
        public void Weight_Conversion()
        {
            const double value = REFERENCE_G;
            var weight = new Weight(value);
            CompareValues(value, weight.AsG);
            CompareValues(GetKg(value), weight.AsKg, 4);
            CompareValues(GetLb(value), weight.AsLb, 4);
        }

        #region Operators

        [Fact]
        public void Operator_Plus()
        {
            CheckOperatorPlus(OPERAND1, OPERAND2);
            CheckOperatorPlus(NEGATIVE_OPERAND1, OPERAND2);
            CheckOperatorPlus(NEGATIVE_OPERAND1, NEGATIVE_OPERAND2);
        }

        [Fact]
        public void Operator_Minus()
        {
            // switch operand due to minus value
            CheckOperatorMinus(OPERAND1, OPERAND2);
            CheckOperatorMinus(OPERAND2, OPERAND1);
            CheckOperatorMinus(NEGATIVE_OPERAND1, OPERAND2);
            CheckOperatorMinus(OPERAND2, NEGATIVE_OPERAND1);
            CheckOperatorMinus(NEGATIVE_OPERAND1, NEGATIVE_OPERAND2);
        }

        [Fact]
        public void Operator_Multiply()
        {
            CheckOperatorMultiply(OPERAND1, OPERAND2);
            CheckOperatorMultiply(NEGATIVE_OPERAND1, OPERAND2);
            CheckOperatorMultiply(NEGATIVE_OPERAND1, NEGATIVE_OPERAND2);
        }

        [Fact]
        public void Operator_Divide()
        {
            CheckOperatorDivide(OPERAND1, OPERAND2);
            CheckOperatorDivide(OPERAND2, OPERAND1);
            CheckOperatorDivide(NEGATIVE_OPERAND1, OPERAND2);
            CheckOperatorDivide(NEGATIVE_OPERAND1, NEGATIVE_OPERAND2);
            CheckOperatorDivide(0, OPERAND2);

#pragma warning disable IDE0039 // Use local function
            Action act = () => CheckOperatorDivide(OPERAND1, 0);
#pragma warning restore IDE0039 // Use local function
            Assert.Throws<ArgumentException>(act);
        }

        [Fact]
        public void Operator_LessThan()
        {
            Assert.True(CheckOperatorLess(SmallerNumber, GreaterNumber));
            Assert.False(CheckOperatorLess(GreaterNumber, SmallerNumber));
            Assert.False(CheckOperatorLess(GreaterNumber, GreaterNumber));
        }

        [Fact]
        public void Operator_LessThanOrEqual()
        {
            Assert.True(CheckOperatorLessOrEqual(SmallerNumber, GreaterNumber));
            Assert.False(CheckOperatorLessOrEqual(GreaterNumber, SmallerNumber));
            Assert.True(CheckOperatorLessOrEqual(GreaterNumber, GreaterNumber));
        }

        [Fact]
        public void Operator_GreaterThan()
        {
            Assert.False(CheckOperatorGreater(SmallerNumber, GreaterNumber));
            Assert.True(CheckOperatorGreater(GreaterNumber, SmallerNumber));
            Assert.False(CheckOperatorGreater(GreaterNumber, GreaterNumber));
        }

        [Fact]
        public void Operator_GreaterThanOrEqual()
        {
            Assert.False(CheckOperatorGreaterOrEqual(SmallerNumber, GreaterNumber));
            Assert.True(CheckOperatorGreaterOrEqual(GreaterNumber, SmallerNumber));
            Assert.True(CheckOperatorGreaterOrEqual(GreaterNumber, GreaterNumber));
        }

        [Fact]
        public void Operator_Equal()
        {
            Assert.True(CheckOperatorEqual(SmallerNumber, SmallerNumber));
            Assert.True(CheckOperatorEqual(NEGATIVE_OPERAND1, NEGATIVE_OPERAND1));
            Assert.False(CheckOperatorEqual(SmallerNumber, GreaterNumber));
            Assert.False(CheckOperatorEqual(OPERAND1, NEGATIVE_OPERAND1));
        }

        [Fact]
        public void Operator_NotEqual()
        {
            Assert.False(CheckOperatorNotEqual(SmallerNumber, SmallerNumber));
            Assert.False(CheckOperatorNotEqual(NEGATIVE_OPERAND1, NEGATIVE_OPERAND1));
            Assert.True(CheckOperatorNotEqual(SmallerNumber, GreaterNumber));
            Assert.True(CheckOperatorNotEqual(OPERAND1, NEGATIVE_OPERAND1));
        }

        #region Private helpers

        private static void CheckOperatorPlus(double op1, double op2)
        {
            var expectedResult = op1 + op2;
            CompareValues(expectedResult, (new Weight(op1) + new Weight(op2)).AsG);
            CompareValues(expectedResult, (new Weight(op1) + op2).AsG);
            CompareValues(expectedResult, (op1 + new Weight(op2)).AsG);
        }

        private static void CheckOperatorMinus(double op1, double op2)
        {
            var expectedResult = op1 - op2;
            CompareValues(expectedResult, (new Weight(op1) - new Weight(op2)).AsG);
            CompareValues(expectedResult, (new Weight(op1) - op2).AsG);
            CompareValues(expectedResult, (op1 - new Weight(op2)).AsG);
        }

        private static void CheckOperatorMultiply(double op1, double op2)
        {
            var expectedResult = op1 * op2;
            CompareValues(expectedResult, (new Weight(op1) * new Weight(op2)).AsG);
            CompareValues(expectedResult, (new Weight(op1) * op2).AsG);
            CompareValues(expectedResult, (op1 * new Weight(op2)).AsG);
        }

        private static void CheckOperatorDivide(double op1, double op2)
        {
            var expectedResult = op1 / op2;
            CompareValues(expectedResult, (new Weight(op1) / new Weight(op2)).AsG);
            CompareValues(expectedResult, (new Weight(op1) / op2).AsG);
            CompareValues(expectedResult, (op1 / new Weight(op2)).AsG);
        }

        private static bool CheckOperatorLess(double op1, double op2)
        {
            return new Weight(op1) < new Weight(op2) &&
                   new Weight(op1) < op2 &&
                   op1 < new Weight(op2);
        }

        private static bool CheckOperatorLessOrEqual(double op1, double op2)
        {
            return new Weight(op1) <= new Weight(op2) &&
                   new Weight(op1) <= op2 &&
                   op1 <= new Weight(op2);
        }

        private static bool CheckOperatorGreater(double op1, double op2)
        {
            return new Weight(op1) > new Weight(op2) &&
                   new Weight(op1) > op2 &&
                   op1 > new Weight(op2);
        }

        private static bool CheckOperatorGreaterOrEqual(double op1, double op2)
        {
            return new Weight(op1) >= new Weight(op2) &&
                   new Weight(op1) >= op2 &&
                   op1 >= new Weight(op2);
        }

        private static bool CheckOperatorEqual(double op1, double op2)
        {
            return new Weight(op1) == new Weight(op2) &&
                   new Weight(op1) == op2 &&
                   op1 == new Weight(op2);
        }

        private static bool CheckOperatorNotEqual(double op1, double op2)
        {
            return new Weight(op1) != new Weight(op2) &&
                    new Weight(op1) != op2 &&
                    op1 != new Weight(op2);
        }

        #endregion

        #endregion

        [Fact]
        public void DecimalPrecision()
        {
            const double c1 = 10.04;
            const double c2 = c1 + 0.0005;

            Weight.GDecimalPrecision = 2;
            Assert.True(CheckOperatorEqual(c1, c2));
            Assert.False(CheckOperatorNotEqual(c1, c2));
            Assert.True(CheckOperatorLessOrEqual(c2, c1));
            Assert.False(CheckOperatorLess(c1, c2));
            Assert.True(CheckOperatorGreaterOrEqual(c1, c2));
            Assert.False(CheckOperatorGreater(c2, c1));

            // change precision
            Weight.GDecimalPrecision = 5;
            Assert.False(CheckOperatorEqual(c1, c2));
            Assert.True(CheckOperatorNotEqual(c1, c2));
            Assert.False(CheckOperatorLessOrEqual(c2, c1));
            Assert.True(CheckOperatorLess(c1, c2));
            Assert.False(CheckOperatorGreaterOrEqual(c1, c2));
            Assert.True(CheckOperatorGreater(c2, c1));
        }

        [Fact]
        public void EqualsTest()
        {
            var timeStamp = DateTimeOffset.Now;
            var obj1 = GetWeightObject(100, timeStamp);
            var obj2 = GetWeightObject(100, timeStamp);
            var obj3 = GetWeightObject(250);

            Assert.True(obj1.Equals(obj2));
            Assert.False(obj1.Equals(obj3));
            Assert.False(obj1.Equals(null));
            object o = 5;
            Assert.False(obj1.Equals(o));
        }

        [Fact]
        public void CompareTo()
        {
            const double baseValue = 100.0;
            const double lessValue = 50.0;
            const double greaterValue = 300.0;
            var timeStamp = DateTimeOffset.Now;

            var obj1 = GetWeightObject(baseValue, timeStamp);
            var obj2 = GetWeightObject(baseValue, timeStamp);
            var obj3 = GetWeightObject(lessValue, timeStamp);
            var obj4 = GetWeightObject(greaterValue, timeStamp);
            var obj5 = GetWeightObject(baseValue, timeStamp.AddSeconds(1));
            //compare weight
            Assert.Equal(0, obj1.CompareTo(baseValue));
            Assert.Equal(1, obj1.CompareTo(lessValue));
            Assert.Equal(-1, obj1.CompareTo(greaterValue));
            Assert.Equal(1, obj1.CompareTo(obj3));
            Assert.Equal(-1, obj1.CompareTo(obj4));

            //compare TimeStamp
            Assert.Equal(0, obj1.CompareTo(obj2));
            Assert.Equal(-1, obj1.CompareTo(obj5));
        }

        #region Private helpers

        private static Weight GetWeightObject(double weight)
        {
            return new Weight(weight);
        }

        private static Weight GetWeightObject(double weight, DateTimeOffset timeStamp)
        {
            return new Weight(weight, timeStamp);
        }

        private static void CompareValues(double expected, double actual, int precision)
        {
            Assert.Equal(Math.Round(expected, precision), Math.Round(actual, precision));
        }

        private static void CompareValues(double expected, double actual)
        {
            CompareValues(expected, actual, Weight.GDecimalPrecision);
        }

        private static double GetLb(double grams)
        {
            return grams / GRAMS_IN_ONE_LIBRA;
        }

        private static double GetKg(double grams)
        {
            return grams / 1000;
        }

        #endregion
    }
}
