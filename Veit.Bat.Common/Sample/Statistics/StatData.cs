﻿using System.Text;
using Veit.Bat.Common.Sample.Ammonia;
using Veit.Bat.Common.Sample.Co2;
using Veit.Bat.Common.Sample.Humidity;
using Veit.Bat.Common.Sample.Temperature;

namespace Veit.Bat.Common.Sample.Statistics
{
    public class StatData : Statistics
    {

        /// <summary>
        /// Daily gain
        /// </summary>
        public float Gain { get; set; }

        /// <summary>
        /// Temperature
        /// </summary>
        public TemperatureSample Temperature { get; set; }

        /// <summary>
        /// Carbon dioxide
        /// </summary>
        public Co2Sample CarbonDioxide { get; set; }

        /// <summary>
        /// Ammonia
        /// </summary>
        public AmmoniaSample Ammonia { get; set; }

        /// <summary>
        /// Humidity
        /// </summary>
        public HumiditySample Humidity { get; set; }

        /// <inheritdoc />
        public override string ToString()
        {
            var sb = new StringBuilder(0x100);
            sb.Append(base.ToString()).Append(" ");
            sb.Append("Gain ").Append(Gain);
            return sb.ToString();
        }
    }
}
