﻿namespace Veit.Bat.Common
{
   public static class DeviceHash
   {
      public static long Bat2 { get { return (long)'B' << 24 | (long)'2' << 16 | (long)'M' << 8; } }
      public static long SensorPack { get { return (long)'S' << 24 | (long)'P' << 16 | (long)'M' << 8; } }
      public static long ExternalSensor { get { return (long)'E' << 56 | (long)'X' << 48; } }
   }
}
