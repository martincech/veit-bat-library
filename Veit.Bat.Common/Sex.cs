﻿namespace Veit.Bat.Common
{
   public enum Sex
   {
      Male,
      Female,
      Undefined
   }
}
