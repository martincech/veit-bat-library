﻿using System;
using System.Runtime.Serialization;

namespace Veit.Bat.Common.Sample.Base
{
    [DataContract]
    public abstract class TimeSample
    {
        [DataMember(Order = 1)]
        public DateTimeOffset TimeStamp { get; set; }

        [DataMember(Order = 2)]
        public string SensorUid { get; set; }

        #region Overrides of Object

        /// <summary>
        /// Returns a string that represents the current object.
        /// </summary>
        /// <returns>
        /// A string that represents the current object.
        /// </returns>
        public override string ToString()
        {
            //time stamp in ISO 8601
            return $"{TimeStamp.ToString("o")} D:{SensorUid}";
        }

        #endregion
    }
}
