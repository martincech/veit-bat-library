﻿using System.Runtime.Serialization;
using Veit.Bat.Common.Units;

namespace Veit.Bat.Common.Sample.Weight
{
    [DataContract]
    public class CalibratedWeight : Weight
    {
        /// <summary>
        /// Init weight in grams
        /// </summary>
        /// <param name="grams">initialization value of weight as grams</param>
        public CalibratedWeight(double grams = 0) : base(grams)
        {
        }

        public CalibratedWeight(Weight weight) : base(weight)
        {
        }

        public CalibratedWeight(double? grams) : base(grams)
        {
        }

        public CalibratedWeight(double weight, WeightUnits units) : base(weight, units)
        {
        }
    }
}
