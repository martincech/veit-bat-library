﻿using Veit.Bat.Common.Sample.Weight;

namespace Veit.Bat.Common.Units.Conversion
{
    /// <summary>
    /// Weight conversions (kg-g-lb)
    /// </summary>
    public static class ConvertWeight
    {
        /// <summary>
        /// Convert weight from one WeightUnits to another
        /// </summary>
        /// <param name="weight">Weight in old WeightUnits</param>
        /// <param name="oldWeightUnits">Old WeightUnits</param>
        /// <param name="newWeightUnits">New WeightUnits</param>
        /// <returns>Weight converted to new WeightUnits</returns>
        public static double Convert(double weight, WeightUnits oldWeightUnits, WeightUnits newWeightUnits)
        {
            var w = new Weight(weight, oldWeightUnits);
            return w.As(newWeightUnits);
        }

        public static double? Convert(double? weight, WeightUnits oldWeightUnits, WeightUnits newWeightUnits)
        {
            if (weight == null) return null;
            return Convert(weight.Value, oldWeightUnits, newWeightUnits);
        }
    }
}
