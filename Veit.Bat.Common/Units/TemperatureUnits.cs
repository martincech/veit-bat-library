﻿namespace Veit.Bat.Common.Units
{
   public enum TemperatureUnits
   {
      Celsius,
      Fahrenheit,
      Kelvin
   }
}
