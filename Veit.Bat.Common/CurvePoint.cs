﻿using Veit.Bat.Common.Sample.Weight;

namespace Veit.Bat.Common
{
   public class CurvePoint
   {
      public CurvePoint(int x, Weight y)
      {
         X = x;
         Y = y;
      }

      public int X { get; set; }
      public Weight Y { get; set; }

      /// <summary>
      /// Compare function
      /// </summary>
      /// <param name="p1">First parameter</param>
      /// <param name="p2">Second parameter</param>
      /// <returns>1, -1 or 0</returns>
      public static int CompareByDay(CurvePoint p1, CurvePoint p2)
      {
         if (p1.X < p2.X)
         {
            return -1;
         }
         return p1.X > p2.X ? 1 : 0;
      }
   }
}
