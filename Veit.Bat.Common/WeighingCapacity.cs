﻿namespace Veit.Bat.Common
{
   public enum WeighingCapacity
   {
      NORMAL, // 30kg/60lb
      EXTENDED, // 50kg/100lb
   };
}
