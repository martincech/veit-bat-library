﻿using System;
using System.Runtime.Serialization;
using Veit.Bat.Common.Sample.Base;
using Veit.Bat.Common.Units;

namespace Veit.Bat.Common.Sample.Weight
{
   [DataContract]
   public class Weight : TimeStampedSample, IComparable
   {
      public static int GDecimalPrecision = 2;
      private const int DECIMAL_POINTS = 4;

      #region Private

      private const string DIVIDE_BY_ZERO = "Divide by zero";
      private const double LB_TO_G_MULTIPLIER = 453.59237;
      private const double KG_TO_G_MULTIPLIER = 1000;

      private static double PRECISION
      {
         get { return Math.Pow(10, -GDecimalPrecision); }
      }

      #endregion

      #region Constructors

      public Weight(double weight, WeightUnits units)
         : base(weight)
      {
         Initialize(weight, units);
      }
      public Weight()
         : this(0)
      {
      }

      public Weight(double grams, DateTimeOffset timeStamp)
         : base(grams, timeStamp)
      {
         Initialize(grams, WeightUnits.G);
      }

      public Weight(double grams)
         : this(grams, WeightUnits.G)
      {

      }

      public Weight(double? grams)
         : this(grams ?? 0)
      {

      }
      public Weight(Weight weight)
         : this(weight == null ? 0 : weight.AsG)
      {
      }

      private void Initialize(double weight, WeightUnits units)
      {
         double value;
         switch (units)
         {
            case WeightUnits.KG:
               value = weight * KG_TO_G_MULTIPLIER;
               break;
            case WeightUnits.G:
               value = weight;
               break;
            case WeightUnits.LB:
               value = weight * LB_TO_G_MULTIPLIER;
               break;
            default:
               throw new ArgumentOutOfRangeException(nameof(units));
         }
         Value = Math.Round(value, GDecimalPrecision, MidpointRounding.AwayFromZero);
      }

      #endregion

      public double RoundedAs(WeightUnits unit)
      {
         return Math.Round(As(unit), DECIMAL_POINTS);
      }

      public double As(WeightUnits unit)
      {
         switch (unit)
         {
            case WeightUnits.KG:
               return AsG / KG_TO_G_MULTIPLIER;
            case WeightUnits.G:
               return AsG;
            case WeightUnits.LB:
               return AsG / LB_TO_G_MULTIPLIER;
            default:
               throw new ArgumentOutOfRangeException(nameof(unit));
         }
      }

      public double AsG
      {
         get { return Value; }
      }

      public double AsKg
      {
         get { return As(WeightUnits.KG); }
      }

      public double AsLb
      {
         get { return As(WeightUnits.LB); }
      }

      #region Operators

      public static Weight operator +(Weight c1, Weight c2)
      {
         return new Weight(c1.Value + c2.Value);
      }

      public static Weight operator +(Weight c1, double c2)
      {
         return new Weight(c1.Value + c2);
      }

      public static Weight operator +(double c1, Weight c2)
      {
         return c2 + c1;
      }

      public static Weight operator -(Weight c1, Weight c2)
      {
         return new Weight(c1.Value - c2.Value);
      }

      public static Weight operator -(Weight c1, double c2)
      {
         return new Weight(c1.Value - c2);
      }

      public static Weight operator -(double c1, Weight c2)
      {
         return new Weight(c1 - c2.Value);
      }

      public static Weight operator *(Weight c1, Weight c2)
      {
         return new Weight(c1.Value * c2.Value);
      }

      public static Weight operator *(Weight c1, double c2)
      {
         return new Weight(c1.Value * c2);
      }

      public static Weight operator *(double c1, Weight c2)
      {
         return c2 * c1;
      }

      public static Weight operator /(Weight c1, Weight c2)
      {
         if (Math.Abs(c2.Value) < PRECISION) throw new ArgumentException(DIVIDE_BY_ZERO);
         return new Weight(c1.Value / c2.Value);
      }

      public static Weight operator /(Weight c1, double c2)
      {
         if (Math.Abs(c2) < PRECISION) throw new ArgumentException(DIVIDE_BY_ZERO);
         return new Weight(c1.Value / c2);
      }

      public static Weight operator /(double c1, Weight c2)
      {
         if (Math.Abs(c2.Value) < PRECISION) throw new ArgumentException(DIVIDE_BY_ZERO);
         return new Weight(c1 / c2.Value);
      }

      public static bool operator <=(Weight c1, Weight c2)
      {
         return c1.Value <= c2.Value;
      }

      public static bool operator <=(Weight c1, double c2)
      {
         return c1.Value <= Round(c2);
      }

      public static bool operator <=(double c1, Weight c2)
      {
         return Round(c1) <= c2.Value;
      }

      public static bool operator >=(Weight c1, Weight c2)
      {
         return c1.Value >= c2.Value;
      }

      public static bool operator >=(Weight c1, double c2)
      {
         return c1.Value >= Round(c2);
      }

      public static bool operator >=(double c1, Weight c2)
      {
         return Round(c1) >= c2.Value;
      }

      public static bool operator >(Weight c1, Weight c2)
      {
         return c1.Value > c2.Value;
      }

      public static bool operator >(Weight c1, double c2)
      {
         return c1.Value > Round(c2);
      }

      public static bool operator >(double c1, Weight c2)
      {
         return c2 < c1;
      }

      public static bool operator <(Weight c1, Weight c2)
      {
         return c1.Value < c2.Value;
      }

      public static bool operator <(Weight c1, double c2)
      {
         return c1.Value < Round(c2);
      }

      public static bool operator <(double c1, Weight c2)
      {
         return c2 > c1;
      }

      public static bool operator ==(Weight c1, Weight c2)
      {
         if (ReferenceEquals(c1, c2)) return true;
         if (c1 is null) return false;
         if (c2 is null) return false;
         return Math.Abs(c1.Value - c2.Value) < PRECISION;
      }

      public static bool operator ==(Weight c1, double c2)
      {
         if (c1 is null) return false;
         return Math.Abs(c1.Value - c2) < PRECISION;
      }

      public static bool operator ==(double c1, Weight c2)
      {
         return c2 == c1;
      }

      public static bool operator !=(Weight c1, Weight c2)
      {
         return !(c1 == c2);
      }

      public static bool operator !=(Weight c1, double c2)
      {
         return !(c1 == c2);
      }

      public static bool operator !=(double c1, Weight c2)
      {
         return !(c1 == c2);
      }

      #endregion

      #region Overrides of Object
      #region Equality members

      protected bool Equals(Weight other)
      {
         return base.Equals(other);
      }

      /// <summary>
      /// Determines whether the specified <see cref="T:System.Object"/> is equal to the current <see cref="T:System.Object"/>.
      /// </summary>
      /// <returns>
      /// true if the specified object  is equal to the current object; otherwise, false.
      /// </returns>
      /// <param name="obj">The object to compare with the current object. </param>
      public override bool Equals(object obj)
      {
         if (obj is null) return false;
         if (ReferenceEquals(this, obj)) return true;
         return obj.GetType() == GetType() && Equals((Weight)obj);
      }

      /// <summary>
      /// Serves as a hash function for a particular type.
      /// </summary>
      /// <returns>
      /// A hash code for the current <see cref="T:System.Object"/>.
      /// </returns>
      public override int GetHashCode()
      {
         unchecked
         {
            return (Value.GetHashCode() * 397) ^ TimeStamp.GetHashCode();
         }
      }

      #endregion

      public int CompareTo(object obj)
      {
         var objT = obj.GetType();
         var objTIsDouble = objT == typeof(double);
         if (objT != GetType() && !objTIsDouble)
         {
            throw new InvalidOperationException($"Cannot compare weight with type {objT}");
         }
         var cmpValue = objTIsDouble ? obj : ((Weight)obj).AsG;
         var weightCmp = ((IComparable)Value).CompareTo(cmpValue);
         if (objTIsDouble)
         {
            return weightCmp;
         }
         return weightCmp == 0 ? ((IComparable)TimeStamp).CompareTo(((Weight)obj).TimeStamp) : weightCmp;
      }

      private static double Round(double c)
      {
         return Math.Round(c, GDecimalPrecision);
      }

      #endregion
   }
}
