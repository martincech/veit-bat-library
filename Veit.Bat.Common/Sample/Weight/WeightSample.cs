﻿using System;
using System.Runtime.Serialization;
using Veit.Bat.Common.Sample.Base;
using Veit.Bat.Common.Units;
using Veit.Bat.Common.Units.Conversion;

namespace Veit.Bat.Common.Sample.Weight
{
   [DataContract]
   public class WeightSample : TimeStampedSample
   {
      private const double WEIGHT_RAW_CONSTANT = 10.0;

      public WeightSample(double rawWeight, DateTimeOffset timeStamp)
         : base(rawWeight, timeStamp)
      {
      }

      public WeightSample(double rawWeight)
         : base(rawWeight)
      {

      }

      public WeightSample(DateTimeOffset timeStamp)
         : base(timeStamp)
      {
      }

      public WeightSample()
      {
      }

      public double ToUnit(WeightUnits toWeightUnit)
      {
         return ConvertWeight.Convert(Value / WEIGHT_RAW_CONSTANT, WeightUnits.G, toWeightUnit);
      }

      public void FromUnit(double value, WeightUnits unit)
      {
         Value = (int)(ConvertWeight.Convert(value, unit, WeightUnits.G) * WEIGHT_RAW_CONSTANT);
      }
   }
}
