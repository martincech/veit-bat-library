﻿using System;
using System.Runtime.Serialization;
using Veit.Bat.Common.Sample.Base;

namespace Veit.Bat.Common.Sample.Humidity
{
   [DataContract]
   public class HumiditySample : TimeStampedSample
   {
      public HumiditySample(double value, DateTimeOffset timeStamp)
         : base(value, timeStamp)
      {
      }

      public HumiditySample(double value)
         : base(value)
      {
      }

      public HumiditySample(DateTimeOffset timeStamp)
         : base(timeStamp)
      {
      }

      public HumiditySample()
      {
      }
   }
}
