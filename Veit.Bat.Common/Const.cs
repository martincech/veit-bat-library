﻿namespace Veit.Bat.Common
{
   public class Const
   {
      public const int PASSWORD_LENGTH = 4;
      public const int BACKLIGHT_MAX = 9;
      public const int CONTRAST_MAX = 64;
      public const int VOLUME_MAX = 9;
      public const int BACKLIGHT_DURATION_MAX = 99;
      public const int POWER_OFF_TIMEOUT_MAX = 99*60;
      public const int FILTER_MAX = 50;
      public const int STABILISATION_TIME_MAX = 50;
      public const int DIRECTORY_SIZE = 199; // files & groups count
      public const int UNITS_KG_RANGE = 30000;
      public const int UNITS_KG_EXT_RANGE = 50000; // 50kg verze
      public const int UNITS_G_RANGE = 30000;
      public const int UNITS_G_EXT_RANGE = 50000;
      public const int UNITS_LB_RANGE = 60000;
      public const int UNITS_LB_EXT_RANGE = 99999;
      public const int TEXT_LENGTH_MAX = 15; // Max 15 znaku textu
   }
}
