﻿using System;
using System.Text;

namespace Veit.Bat.Common.Sample.Statistics
{
    /// <summary>
    ///    Statistics of one day
    /// </summary>
    public class Statistics
    {
        public Statistics()
        {
            DateOfTheDay = default(DateTimeOffset);
            Histogram = new Histogram();
        }

        /// <summary>
        ///    Date of the day
        /// </summary>
        public DateTimeOffset DateOfTheDay { get; set; }

        /// <summary>
        ///    Day number
        /// </summary>
        public int DayNumber { get; set; }

        /// <summary>
        ///    Target weight
        /// </summary>
        public float Target { get; set; }

        /// <summary>
        ///    Number of birds weighed
        /// </summary>
        public int Count { get; set; }

        /// <summary>
        ///    Average weight
        /// </summary>
        public float Average { get; set; }

        /// <summary>
        ///    Average from previous day
        /// </summary>
        public float LastAverage { get; set; }

        /// <summary>
        ///    Standard deviation
        /// </summary>
        public float Sigma { get; set; }

        /// <summary>
        ///    Uniformity in %
        /// </summary>
        public int Uniformity { get; set; }

        /// <summary>
        ///    Coefficient of variation
        /// </summary>
        public float Cv { get; set; }

        /// <summary>
        ///    Weight distribution of raw samples
        /// </summary>
        public Histogram Histogram { get; set; }

        /// <inheritdoc />
        public override string ToString()
        {
            var sb = new StringBuilder(0x100);
            sb.Append("Day ").Append(DayNumber).Append('(').Append(DateOfTheDay).Append("): ");
            sb.Append("Average ").Append(Average).Append("(Last ").Append(LastAverage).Append(") ");
            sb.Append("Target ").Append(Target).Append(" ");
            sb.Append("Count ").Append(Count).Append(" ");
            sb.Append("Sigma ").Append(Sigma).Append(" ");
            sb.Append("Uniformity ").Append(Uniformity).Append(" ");
            sb.Append("Cv ").Append(Cv).Append(" ");
            sb.Append("Histogram ").Append('(').Append(Histogram?.ToString() ?? "NO DATA").Append(')');
            return sb.ToString();
        }
    }
}
