﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace Veit.Bat.Common.Schema
{

    [JsonConverter(typeof(StringEnumConverter))]
    public enum BirdSex
    {
        [EnumMember(Value = "female")]
        Female,
        [EnumMember(Value = "male")]
        Male,
        [EnumMember(Value = "undefined")]
        Undefined,
    }

    public class Sample
    {
        [Description("Time stamp of sample acquisition.")]
        [JsonProperty("timestamp", Required = Required.Always)]
        public DateTimeOffset TimeStamp { get; set; }

        [Description("Bird Sex.")]
        [JsonProperty("sex", Required = Required.Always)]
        public BirdSex Sex { get; set; }

        [Description("Bird weight in grams.")]
        [JsonProperty("weight", Required = Required.Always)]
        public int Weight { get; set; }
    }

    [JsonObject(Description = "Object representing BAT2 scale including its configuration and statistics", Id = "scale")]
    public class ScaleSamples
    {
#if JSON_SCHEMA_GENERATOR
        [MinLength(36)]
        [MaxLength(36)]
        [RegularExpression("^[0-9a-fA-F]{8}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{12}$")]
#endif // JSON_SCHEMA_GENERATOR

        [JsonProperty("id", Required = Required.Always)]
        [Description("ID of scale represented as UUID.")]
        public Guid Id { get; set; }

        [JsonProperty("displayName", Required = Required.Default)]
        [RegularExpression("^[0-9\\s]{0,15}$")]
        [Description("DisplayName read from scale, consist of numbers and spaces, maximal length is 15. Optional. Null when not set in scale.")]
        [MaxLength(15)]
        public string DisplayName { get; set; }

        [Description("Weighing samples from scale.")]
        [JsonProperty("samples", Required = Required.Always)]
        public List<Sample> Samples { get; set; }
    }

    public class SamplesBody
    {
        [JsonProperty("version", Required = Required.Always)]
        [Range(1, 1)]
        public int Version;

        [JsonProperty("date", Required = Required.Always)]
        public DateTimeOffset DateTime { get; set; }

        [JsonProperty("scales", Required = Required.Always)]
        public List<ScaleSamples> Scales { get; set; }
    }
}
