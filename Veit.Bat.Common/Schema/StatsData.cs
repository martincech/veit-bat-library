﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;

namespace Veit.Bat.Common.Schema
{
    [JsonObject(Description = "Object representing daily statistics values", Id = "curve-point")]
    public class CurvePoint
    {
        [Range(0, 999)]
        [JsonProperty("day", Required = Required.Always)]
        [Description("Day of flock.")]
        public int Day { get; set; }

        [JsonProperty("weight", Required = Required.Always)]
        [Description("Expected weight in grams at the defined day.")]
        [Range(0, 99999)]
        public int Weight { get; set; }
    }


    [JsonObject(Description = "Object representing daily statistics values", Id = "daily-stats")]
    public class DailyStats
    {
        [Range(0, 999)]
        [JsonProperty("day", Required = Required.Always)]
        [Description("Actual day of running flock.")]
        public int Day { get; set; }

        [JsonProperty("date", Required = Required.Always)]
        [Description("Date and Time when the stats have been generated.")]
        public DateTimeOffset DateTime { get; set; }

        [JsonProperty("count", Required = Required.Always)]
        [Description("Number of samples weighted during day.")]
        [Range(0, 9999)]
        public int Count { get; set; }

        [JsonProperty("average", Required = Required.Always)]
        [Description("Average weight in grams.")]
        [Range(0, 99999)]
        public int Avg { get; set; }

        [JsonProperty("gain", Required = Required.Always)]
        [Description("Daily gain in grams.")]
        [Range(-99999, 99999)]
        public int Gain { get; set; }

        [JsonProperty("uni", Required = Required.Always)]
        [Description("Uniformity in percentage.")]
        [Range(0, 100)]
        public double Uniformity { get; set; }

        [JsonProperty("sig", Required = Required.Always)]
        [Description("Standard deviation in grams.")]
        [Range(0, 99999)]
        public int Sig { get; set; }

        [JsonProperty("cv", Required = Required.Always)]
        [Description("Coefficient of variation in percentage.")]
        [Range(0, 100)]
        public double Cv { get; set; }

        [JsonProperty("histogram", Required = Required.Default)]
        [Description("Weight distribution of raw samples.")]
        public Histogram Histogram { get; set; }
    }

    [JsonObject(Description = "Object representing statistics of sex in flock", Id = "sex-stats")]
    public class SexStats
    {
        [JsonProperty("curve", Required = Required.Default)]
        [Description("Growth curve definition for the sex in flock. Null when not defined, e.g. in case of broilers.")]
        public List<CurvePoint> Curve { get; set; }

        [JsonProperty("stats", Required = Required.Always)]
        [Description("List of statistics of flock, daily granularity including whole flock history.")]
        public List<DailyStats> Stats { get; set; }
    }


    [JsonObject(Description = "Object representing Flock including its info and statistics", Id = "flock")]
    public class Flock
    {
        [JsonProperty("males", Required = Required.Default)]
        [Description("Statistics of males in case of sex separation. Null when no separation is configured.")]
        public SexStats Males { get; set; }

        [JsonProperty("females", Required = Required.Always)]
        [Description("Statistics of females in case of sex separation. Mixed sex otherwise.")]
        public SexStats Females { get; set; }
    }

    [JsonObject(Description = "Object representing BAT2 scale including its configuration and statistics", Id = "scale")]
    public class Scale
    {
#if JSON_SCHEMA_GENERATOR
        [MinLength(36)]
        [MaxLength(36)]
        [RegularExpression("^[0-9a-fA-F]{8}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{12}$")]
#endif // JSON_SCHEMA_GENERATOR

        [JsonProperty("id", Required = Required.Always)]
        [Description("ID of scale represented as UUID.")]
        public Guid Id { get; set; }

        [JsonProperty("displayName", Required = Required.Default)]
        [RegularExpression("^[0-9\\s]{0,15}$")]
        [Description("DisplayName read from scale, consist of numbers and spaces, maximal length is 15. Optional. Null when not set in scale.")]
        [MaxLength(15)]
        public string DisplayName { get; set; }

        [Range(0, 999)]
        [JsonProperty("day", Required = Required.Default)]
        [Description("Actual day of running flock. Null when flock not configured or weighing of flock not yet started.")]
        public int? Day { get; set; }

        [JsonProperty("flock", Required = Required.Default)]
        [Description("Actual running flock info. Null when flock not configured or weighing of flock not yet started.")]
        public Flock Flock { get; set; }
    }

    public class StatsBody
    {
        [JsonProperty("version", Required = Required.Always)]
        [Range(1, 1)]
        public int Version;

        [JsonProperty("date", Required = Required.Always)]
        public DateTimeOffset DateTime { get; set; }

        [JsonProperty("scales", Required = Required.Always)]
        public List<Scale> Scales { get; set; }
    }

    [JsonObject(Description = "Object representing BAT2 scale histogram", Id = "histogram")]
    public class Histogram
    {
        [JsonProperty("stepsSize", Required = Required.Always)]
        [Description("Weight difference between adjacent steps in grams.")]
        public int StepsSize { get; set; }

        [JsonProperty("stepsCount", Required = Required.Always)]
        [Description("Total number of steps in historgram.")]
        public int StepsCount { get; set; }

        [JsonProperty("center", Required = Required.Always)]
        [Description("Center value of the histogram in grams.")]
        public int Center { get; set; }

        [JsonProperty("steps", Required = Required.Always)]
        [Description("Dictionary of histogram values. (Key = Step or Weight, Value = Count)")]
        public Dictionary<int, int> Steps { get; set; }
    }
}
