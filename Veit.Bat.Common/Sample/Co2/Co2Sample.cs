﻿using System;
using System.Runtime.Serialization;
using Veit.Bat.Common.Sample.Base;

namespace Veit.Bat.Common.Sample.Co2
{
   [DataContract]
   public class Co2Sample : TimeStampedSample
   {
      public Co2Sample(double value, DateTimeOffset timeStamp)
         : base(value, timeStamp)
      {
      }

      public Co2Sample(double value)
         : base(value)
      {
      }

      public Co2Sample(DateTimeOffset timeStamp)
         : base(timeStamp)
      {
      }

      public Co2Sample()
      {
      }
   }
}
