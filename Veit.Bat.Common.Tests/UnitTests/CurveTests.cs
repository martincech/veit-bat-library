﻿using System.Linq;
using Veit.Bat.Common.Sample.Weight;
using Xunit;

namespace Veit.Bat.Common.Tests.UnitTests
{
    public class CurveTests
    {
        #region Private fields

        private static readonly Curve _curve;
        private const int POINTS_START_X = 5;
        private const int POINTS_STOP_X = 50;
        private const int POINTS_INCREMENT = 5;
        private const int POINTS_VALUE_MULTIPLIER = 10;

        #endregion

        #region Init

        static CurveTests()
        {
            _curve = new Curve();
        }

        public CurveTests()
        {
            _curve.Points.Clear();
            for (var i = POINTS_START_X; i <= POINTS_STOP_X; i += POINTS_INCREMENT)
            {
                _curve.Points.Add(new CurvePoint(i, new Weight(i * POINTS_VALUE_MULTIPLIER)));
            }
        }

        #endregion

        [Fact]
        public void GetCurveValue_EmptyList()
        {
            _curve.Points.Clear();
            Assert.Empty(_curve.Points);
            Assert.Null(_curve.GetCurveValue(10));
        }

        [Fact]
        public void GetCurveValue_ExistingPoint()
        {
            const int valueX = POINTS_START_X + POINTS_INCREMENT;
            const int expectedY = valueX * POINTS_VALUE_MULTIPLIER;

            Assert.Equal(expectedY, _curve.GetCurveValue(valueX).AsG);
        }

        [Fact]
        public void GetCurveValue_NoExistingPoint_Interpolation()
        {
            const int day = (int)(POINTS_START_X + (POINTS_INCREMENT / 2.0));

            var point0 = _curve.Points.ElementAt(0);
            var point1 = _curve.Points.ElementAt(1);

            var interpolationResult = (int)CalcInterpolation(point0.X, point0.Y.AsG, point1.X, point1.Y.AsG, day);
            Assert.Equal(interpolationResult, _curve.GetCurveValue(day).AsG);
        }

        [Fact]
        public void GetCurveValue_NoExistingPoint_DaySmallerThanFirstPoint()
        {
            var value = _curve.GetCurveValue(POINTS_START_X - 1);
            Assert.NotNull(value);
            Assert.True(value < _curve.Points.First().Y);
        }

        [Fact]
        public void GetCurveValue_NoExistingPoint_DayGreaterThanLastPoint()
        {
            var value = _curve.GetCurveValue(POINTS_STOP_X + 1);
            Assert.NotNull(value);
            Assert.Equal(_curve.Points.Last().Y.AsG, value.AsG);
        }

        [Fact]
        public void GetCurveValue_DayInvalidValue()
        {
            Assert.Null(_curve.GetCurveValue(-1));
        }

        #region Private helpers

        private static double CalcInterpolation(double x0, double y0, double x1, double y1, double x)
        {
            var y = y0 + ((y1 - y0) / (x1 - x0)) * (x - x0);
            return y;
        }

        #endregion
    }
}
