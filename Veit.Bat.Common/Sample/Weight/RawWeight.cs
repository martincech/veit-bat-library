﻿using System;
using System.Runtime.Serialization;
using Veit.Bat.Common.Units;

namespace Veit.Bat.Common.Sample.Weight
{
   [DataContract]
   public class RawWeight : Weight
   {
      /// <summary>
      /// Initialize weight in grams
      /// </summary>
      /// <param name="grams">initialization value of weight as grams</param>
      public RawWeight(double grams = 0) : base(grams)
      {
      }

      public RawWeight()
         : base(0)
      {
      }

      public RawWeight(double grams, DateTimeOffset timeStamp)
         : base(grams, timeStamp)
      {
      }

      public RawWeight(Weight weight) : base(weight)
      {
      }

      public RawWeight(double? grams) : base(grams)
      {
      }

      public RawWeight(double weight, WeightUnits units) : base(weight, units)
      {
      }
   }
}
